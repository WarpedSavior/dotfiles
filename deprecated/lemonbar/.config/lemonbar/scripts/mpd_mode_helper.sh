#!/bin/sh

# returns random, single-repeat or consume icons if called with -z, -r or -c
# cycles through repeat -> repeat-single -> off if called with -R

# Icons (should be compatible with POSIX shells like dash)
RANDOM_ICON="\356\216\256"          # \ue3ae
REPEAT_ICON="\356\216\253"          # \ue3ab
REPEAT_SINGLE_ICON="\356\216\254"   # \ue3ac
CONSUME_ICON="\356\216\226"         # \ue396

repeat=$(mpc status | tail -n1 | sed -e 's/[^:]*://' | awk '{print $3}')
random=$(mpc status | tail -n1 | sed -e 's/[^:]*://' | awk '{print $5}')
single=$(mpc status | tail -n1 | sed -e 's/[^:]*://' | awk '{print $7}')
consume=$(mpc status | tail -n1 | sed -e 's/[^:]*://' | awk '{print $9}')

while getopts "zrcR" opt; do
    case ${opt} in
        z)
            if [ "$random" = "on" ]; then           
                printf "%b\n" "$RANDOM_ICON"
            else
                printf "%b\n" "%{F#545454}$RANDOM_ICON%{F-}"
            fi
            ;;
        r)
            if [ "$repeat" = "on" ] && [ "$single" = "on" ]; then
                printf "%b\n" "$REPEAT_SINGLE_ICON"
            elif [ "$repeat" = "on" ] && [ "$single" = "off" ]; then
                printf "%b\n" "$REPEAT_ICON"
            elif [ "$repeat" = "off" ] && [ "$single" = "on" ]; then
                printf "%s\n" "1"
            else
                printf "%b\n" "%{F#545454}$REPEAT_SINGLE_ICON%{F-}"
            fi
            ;;
        c)
            if [ "$consume" = "on" ]; then
                printf "%b\n" "$CONSUME_ICON"
            else
                printf "%b\n" "%{F#545454}$CONSUME_ICON%{F-}"
            fi
            ;;
        R)
            if [ "$repeat" = "off" ] && [ "$single" = "off" ]; then
                mpc repeat >/dev/null
            elif [ "$repeat" = "on" ] && [ "$single" = "off" ]; then
                mpc single >/dev/null
            elif [ "$repeat" = "off" ] && [ "$single" = "on" ]; then
                mpc repeat >/dev/null
            else
                mpc repeat >/dev/null
                mpc single >/dev/null
            fi
            ;;
        *)
            printf "%b\n" "Invalid option: $OPTARG" 1>&2
            ;;
    esac
done
