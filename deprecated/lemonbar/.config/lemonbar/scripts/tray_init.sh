#!/bin/sh

PANEL_FIFO=/tmp/panel.fifo

[ -e "$PANEL_FIFO" ] || mkfifo "$PANEL_FIFO"

# hacky way to "listen" for tray icon events. It will launch stalonetray from here as a side effect
#stalonetray --log-level info 2>&1 | grep --line-buffered "managed" | while read line; do
while :; do

    tail -f -n0 /tmp/stalonetray.log | grep -q -e "------------"
    #sleep 0.5 # Sleep to allow programs to properly close before updating the bar
    STEAM="   "
    FCITX="   "
    UDISKIE="  "
    DISCORD="   "
    NETWORK="   "
    TELEGRAM="   "
    TRANSMISSION="   "
    pgrep -x steam >/dev/null || STEAM=""
    pgrep -x fcitx5 >/dev/null|| FCITX=""
    pgrep -x Discord >/dev/null|| DISCORD=""
    pgrep -x nm-applet >/dev/null|| NETWORK=""
    pgrep -x telegram-deskto >/dev/null|| TELEGRAM=""
    pgrep -x transmission-gt >/dev/null|| TRANSMISSION=""
    pgrep -x udiskie >/dev/null && lsblk -o tran | grep usb || UDISKIE=""
    printf "%b\n" "TRAY${FCITX}${NETWORK}${UDISKIE}${STEAM}${TELEGRAM}${TRANSMISSION}${DISCORD}"

done > "$PANEL_FIFO"
