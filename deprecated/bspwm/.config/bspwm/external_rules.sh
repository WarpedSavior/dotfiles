#!/bin/bash

WID=$1
CLASS=$2
INSTANCE=$3

#Debug
#TITLE=$(xtitle "$WID")
#echo "$WID $CLASS $INSTANCE $TITLE" > /tmp/bspc-external-rules

case $CLASS in
        "Steam")
            if xprop -id "$wid" -len 512 | grep -qs 'program specified maximum size'; then
                xprop -id "$wid" \
                    -f _NET_WM_WINDOW_TYPE 32a \
                    -set _NET_WM_WINDOW_TYPE _NET_WM_WINDOW_TYPE_DIALOG &>/dev/null
                echo "manage=on"
            fi
            TITLE=$(xprop -id "$WID" WM_NAME)
            case $TITLE in
                *Friend*)
                    echo "state=floating follow=off focus=off"
                    ;;
            esac
            ;;
        "scratchterm")
                xdo move -x 0 -y 25 "$WID"
                xdo resize -w 1366 -h 345 "$WID"
                echo "border = off"
                ;;
        "mpdplayer")
                xdo resize -w 663 -h 287 "$WID"
                ;;
esac
