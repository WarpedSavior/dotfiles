/*Dracula theme based on the Purple official rofi theme*/

* {
    font: "monospace 12";
    foreground: #f8f8f2;
    background: #282A36;
    active-background: #BD93F9;
    urgent-background: #ff5555;
    active-foreground: #8BE9FD;
    selected-background: @active-background;
    selected-urgent-background: @urgent-background;
    selected-active-background: @active-background;
    selected-foreground: @background;
    selected-urgent-foreground: @background;
    selected-active-background: @active-foreground;
    urgent-foreground: @urgent-background;
    separatorcolor: @active-background;
    bordercolor: @active-background;
}

configuration {
    drun-display-format:            "{name}";
    drun-match-fields:              "name,exec";
    modi:                           "drun,run,window";
}

@import "keybinds/keybinds.rasi"

#window {
    background-color: @background;
    border:           2;
    border-radius: 6;
    border-color: @bordercolor;
    padding:          5;
}
#mainbox {
    border:  0;
    padding: 0;
}
#message {
    border:       1px dash 0px 0px ;
    border-color: @separatorcolor;
    padding:      1px ;
}
#textbox {
    text-color: @foreground;
}
#listview {
    fixed-height: 0;
    border:       2px dash 0px 0px ;
    border-color: @bordercolor;
    spacing:      2px ;
    scrollbar:    false;
    padding:      2px 0px 0px ;
}
#element {
    border:  0;
    padding: 1px ;
}
#element.normal.normal {
    background-color: @background;
    text-color:       @foreground;
}
#element.normal.urgent {
    background-color: @background;
    text-color:       @urgent-foreground;
}
#element.normal.active {
    background-color: @background;
    text-color:       @active-foreground;
}
#element.selected.normal {
    background-color: @selected-background;
    text-color:       @selected-foreground;
}
#element.selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
#element.selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
#element.alternate.normal {
    background-color: @background;
    text-color:       @foreground;
}
#element.alternate.urgent {
    background-color: @background;
    text-color:       @urgent-foreground;
}
#element.alternate.active {
    background-color: @background;
    text-color:       @active-foreground;
}
#scrollbar {
    width:        2px ;
    border:       0;
    handle-width: 8px ;
    padding:      0;
}
#sidebar {
    border:       2px dash 0px 0px ;
    border-color: @separatorcolor;
}
#button.selected {
    background-color: @selected-background;
    text-color:       @foreground;
}
#inputbar {
    spacing:    0;
    text-color: @foreground;
    padding:    1px ;
}
#case-indicator {
    spacing:    0;
    text-color: @foreground;
}
#entry {
    spacing:    0;
    text-color: @foreground;
}
#prompt {
    spacing:    0;
    text-color: @foreground;
}
#inputbar {
    children:   [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
#textbox-prompt-colon {
    expand:     false;
    str:        ":";
    margin:     0px 0.3em 0em 0em ;
    text-color: @foreground;
}

element-text, element-icon {
    background-color: inherit;
    text-color: inherit;
}
