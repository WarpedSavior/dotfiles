#!/usr/bin/env bash

SCRIPT_DIR=$(dirname $(realpath $0))

LOCK=""
SLEEP=""
LOGOUT=""
RESTART=""
SHUTDOWN=""

list_icons() {
    echo $SHUTDOWN
    echo $RESTART
    echo $LOCK
    echo $SLEEP
    echo $LOGOUT
}

handle_option() {
    case "$1" in
        "$LOCK")
            slock
            ;;
        "$SLEEP")
            slock &
            systemctl suspend
            ;;
        "$LOGOUT")
            killall dwmbar && killall xinit
            ;;
        "$RESTART")
            systemctl reboot
            ;;
        "$SHUTDOWN")
            systemctl poweroff
            ;;
    esac
}

SELECTION="$(list_icons | rofi -dmenu -theme options-menu -selected-row 2)"
handle_option $SELECTION
