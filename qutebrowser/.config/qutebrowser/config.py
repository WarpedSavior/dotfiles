# =======================
# Qutebrowser Config File
# =======================

# Change the argument to True to still load settings configured via autoconfig.yml
config.load_autoconfig(False)

# Require a confirmation before quitting the application.
c.confirm_quit = ['downloads']

# List of URLs to ABP-style adblocking rulesets.
c.content.blocking.adblock.lists = ['https://easylist.to/easylist/easylist.txt', 'https://easylist.to/easylist/easyprivacy.txt']

# Enable the ad/host blocker
c.content.blocking.enabled = True

# Block subdomains of blocked hosts.
c.content.blocking.hosts.block_subdomains = True

# List of URLs to host blocklists for the host blocker.
c.content.blocking.hosts.lists = ['https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts']

# Which method of blocking ads should be used.
c.content.blocking.method = 'auto'

# Allow pdf.js to view PDF files in the browser.
c.content.pdfjs = False

# Automatically enter insert mode if an editable element is focused
# after loading the page.
c.input.insert_mode.auto_load = False

# Leave insert mode when starting a new page load.
c.input.insert_mode.leave_on_load = True

# Languages to use for spell checking.
c.spellcheck.languages = ['en-US', 'es-ES']

# Value to use for `prefers-color-scheme:` for websites.
c.colors.webpage.preferred_color_scheme = 'dark'

# Allow sites to show notifications.
for site in [
        'https://www.reddit.com',
        'https://discord.com'   ,
]:
    config.set('content.notifications.enabled', True, site)

# Search engines which can be used via the address bar.
c.url.searchengines = {
    "DEFAULT": 'https://duckduckgo.com/?q={}',
    "d": 'https://duckduckgo.com/?q={}',
    "r": 'https://www.reddit.com/r/{}',
    "w": 'https://en.wikipedia.org/wiki/={}',
    "y": 'https://www.youtube.com/results?search_query={}',
    "yid": 'https://www.youtube.com/watch?v={}',
    "aw": 'https://wiki.archlinux.org/?search={}',
    "g": 'https://www.google.com/search?hl=en&q={}',
    "am": 'https://www.amazon.es/s?k={}',
    "ub": 'https://www.urbandictionary.com/define.php?term={}',
}

# Handler for selecting file(s) in forms.
c.fileselect.handler = 'external'
c.fileselect.single_file.command = ['st', '-c', 'floating', '-e', 'vifmrun', '--choose-files', '{}']
c.fileselect.multiple_files.command = ['st', '-c', 'floating', '-e', 'vifmrun', '--choose-files', '{}']
c.fileselect.folder.command = ['st', '-c', 'floating', '-e', 'vifmrun', '--choose-dir', '{}']

# Change the start page
c.url.start_pages = '~/.local/share/qutebrowser/the-glorious-startpage/index.html'

# Hints for reddit expando buttons
config.set('hints.selectors', {
    'all': [*c.hints.selectors['all'], '.expando-button'],
}, pattern='*://*.reddit.com/*')

# Per-domain stylesheets
import glob
c.content.user_stylesheets = glob.glob('/home/jos/.local/share/qutebrowser/css/*.user.css')

# Dracula theme
import dracula.draw
dracula.draw.blood(c, {
    'spacing':{
        'vertical':1,
        'horizontal':8
        }
    })


# ========
# Bindings
# ========

# Change the default scrolling to scroll-px
config.bind('j', 'scroll-px 0 50')
config.bind('k', 'scroll-px 0 -50')

# download a video using youtube-dl
config.bind(",y", 'hint links spawn --detach youtube-dl -o "~/Downloads/youtube/%(title)s [%(id)s].%(ext)s" {hint-url}')
# download current video
config.bind(",Y", 'spawn --detach st -e youtube-dl -o "~/Downloads/youtube/%(title)s [%(id)s].%(ext)s" {url}')
# download audio from a video using youtube-dl
config.bind(",x", 'hint links spawn --detach youtube-dl -o "~/Downloads/youtube/%(title)s [%(id)s].%(ext)s" -x {hint-url}')
# download audio from current video
config.bind(",X", 'spawn --detach st -e youtube-dl -o "~/Downloads/youtube/%(title)s [%(id)s].%(ext)s" -x {url}')

# open a url with mpv
config.bind(",m", 'hint links spawn --detach umpv {hint-url}')
# open the current url with mpv
config.bind(",M", 'spawn --detach umpv {url}')

# open the current url in firefox
config.bind(',F', 'spawn firefox --private-window {url}')

# search and translate selected text
config.bind(",f", 'open --tab {primary}')
config.bind(",t", 'open --tab https://translate.google.com/#auto/es/{primary}')

# search a web in the internet archive
config.bind(",wa", 'open -t https://web.archive.org/web/{url}')

# password management
config.bind(",p", 'spawn --userscript qute-pass')
config.bind(",P", 'spawn --userscript qute-pass --password-only')

# delete hinted target
config.bind("D", 'hint all delete')

# Edit url in external editor
# --cmd as a workaround for the autopairs plugin remapping <CR>
config.bind("ge", 'edit-url')
c.editor.command = ['st', '-e', 'vim', '-S', '~/.config/qutebrowser/editor.vimrc', '--cmd', 'let g:AutoPairsMapCR = 0', '{}']

# Toggle adblocking on/off
config.bind(',b', 'config-cycle content.blocking.enabled')
