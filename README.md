# dotfiles

![pic](desktop.png)
![pic](desktop2.png)

repo containing my config files for the Arch Linux distribution.

```
 bash         -> bash settings, aliases, etc.
 colorschemes -> includes some color themes to use with Xresources
 dunst        -> daemon notification config
 dwm          -> window manager, hotkey daemon and tray configuration
 gsimplecal   -> light calendar applet to use with the bar
 mail         -> mutt + isync to manage mail locally
 mpv          -> video player (includes some scripts as well)
 music        -> contains the configuration for mpd and ncmpcpp
 nsxiv        -> image viewer
 picom        -> composite manager for x11
 qutebrowser  -> keyboard-driven browser
 redshift     -> for night owls
 rofi         -> application launcher
 scripts      -> includes some useful scripts
 udiskie      -> automounter for removable media
 vifm         -> ncurses based file manager with vi like keybindings
 vim          -> vim configuration
 x11          -> x11 settings
 xob          -> overlay volume/backlight bar for X11
 zathura      -> document viewer
```

I'm using [GNU Stow](https://www.gnu.org/software/stow/) to manage my files easily with the help of the install.sh script. Note that the script is meant for personal use only, and even though it won't delete any files (in case of conflict the original files will be renamed with the .bak extension), it's recommended to manually grab the piece of config/code you want instead.

If installing the dotfiles is desired, first clone the repository:

    $ git clone --depth=1 'https://gitlab.com/WarpedSavior/dotfiles.git'

Now, change the working directory:

    $ cd dotfiles/

Finally, run the script that will take care of everything else:

    $ ./install.sh

That's it!
