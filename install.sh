#!/usr/bin/env bash

# https://zihao.me/post/managing-dotfiles-with-gnu-stow/
# The script will try to install the packages to their respective location,
# backing up all the files that would produce conflicts
PACKAGES=(
    bash
    colorschemes
    dunst
    dwm
    git
    gsimplecal
    mpv
    music
    nsxiv
    picom
    qutebrowser
    redshift
    rofi
    scripts
    udiskie
    vifm
    vim
    x11
    xob
    zathura
)

if ! command -v stow >/dev/null; then
    echo "You should install GNU Stow first!"
    echo "https://www.gnu.org/software/stow/"
    exit 1
fi

for PKG in ${PACKAGES[@]}; do
    CONFLICTS=$(stow --no --verbose $PKG 2>&1 | awk '/\* existing target is/ {print $NF}')
    for filename in ${CONFLICTS[@]}; do
        if [[ -f $HOME/$filename || -L $HOME/$filename ]]; then
            echo "BACKING UP: $filename"
            mv "$HOME/$filename" "$HOME/$filename.bak"
        fi
    done

    stow --no-folding --verbose $PKG
done
