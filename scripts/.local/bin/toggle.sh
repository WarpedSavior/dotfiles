#!/bin/sh
# Toggle applications that have a specific WM_CLASS or WM_NAME
# Select the title or class to search for via --title or --class, and the command
# that will be run (that could be 'st -c scratchpad' or 'steam')
# Its main use case is to create scratchpad-like windows that will be hidden or shown
# with a keybind, but GUI applications, or applications that have their own title
# or class should also be supported, although they are more hit or miss and could
# require extra steps.

usage() {
    echo "Usage: $0 [OPTIONS] -- [COMMAND]
    Where OPTIONS can be one of the following:
        --class: Search by WM_CLASS for an existing window.
        --title: Search by WM_NAME for an existing window.

    Both options can be specified, but class will take
    precedence over title.

    Use (optionally) -- to make a distinction between
    the options and the command that will be executed.

    Finally, COMMAND is the program (with or without extra arguments)
    that will be toggled."
    exit 1
}

# getopts does not support long options, so we convert them to short options.
for arg in "$@"; do
    shift
    case "$arg" in
        --class) set -- "$@" '-i' ;;
        --title) set -- "$@" '-p' ;;
        *) set -- "$@" "$arg" ;;
    esac
done

while getopts "hi:p:" opt; do
    case ${opt} in
        i) class="$OPTARG" ;;
        p) title="$OPTARG" ;;
        h) usage ;;
        \?) usage ;;
        :) usage ;;
    esac
done

shift "$(( OPTIND - 1 ))"

# Create ishidden.pid if it doesn't exist and check if the scratchpad is running.
[ -f /tmp/ishidden.id ] || touch /tmp/ishidden.id

if [ -n "$class" ]; then
    wid=$(xdo id -N "$class" | head -n1)
elif [ -n "$title" ]; then
    wid=$(xdo id -a "$title" | head -n1)
else
    usage
fi

# If it's running, toggle it. If it isn't, then run it.
if [ -n "$wid" ]; then
    # since xdo can't tell if a window is hidden or not, we use a tmp file
    if grep -qw "$wid" /tmp/ishidden.id; then
        xdo show "$wid"
        sed -i "/$wid/d" /tmp/ishidden.id
    else
        xdo hide "$wid"
        echo "$wid" >> /tmp/ishidden.id
    fi
else
    # Verify that the command passed as argument exists and run it
    command -v "$1" >/dev/null || usage

    "$@" >/dev/null 2>&1 &
fi
