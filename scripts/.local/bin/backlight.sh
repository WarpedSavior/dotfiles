#!/bin/sh
# Increase/decrease backlight,
# and send the updated value to the xob named pipe

usage() {
    cat <<EOF
Usage: $0 -i arg | -d arg | -h
where:
    -i increase backlight arg%
    -d decrease backlight arg%
    -h show this help message
EOF
    exit 0
}

while getopts ":i:d:h" opt; do
    case ${opt} in
        h)  # show the usage
            usage
            ;;

        i)  # increase backlight
            light -A $OPTARG
            light -G | awk '{print int($1+0.5)}' >> /tmp/xob_backlight
            ;;
    
        d)  # decrease volume
            light -U $OPTARG
            light -G | awk '{print int($1+0.5)}' >> /tmp/xob_backlight
            ;;
    
        \?) # unknown optarg
            echo "Invalid option: -$OPTARG" 1>&2
            exit 1
            ;;

        : )  # optarg without argument
            echo "Option -"OPTARG" requires an argument." 1>&2
            exit 1
            ;;
    esac
done
