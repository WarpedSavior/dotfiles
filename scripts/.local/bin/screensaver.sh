#!/bin/sh
# Run programs when no activity is detected. It uses 2 timers to achieve that.
# The first timer will run asciiquarium as a screensaver, it's recommended to
# tell your wm to fullscreen it.
# The second timer will close the asciiquarium window, will lock the screen, 
# and will turn the monitor off.
# (note that as long as the screen is locked, asciiquarium won't be run again).

screensaver=false
idleExtra=0
timer_1=300     # run asciiquarium after 5 minutes
timer_2=1200    # lock screen after 20 minutes

while :; do
    idleTime=$(( $(xprintidle) / 1000 ))
    sound=$(pactl list | grep -c "State: RUNNING")
    # if asciiquarium isn't running, and there's an active window, check if the current window is fullscreen
    aquarium_wid=$(xdo id -N aquarium)
    active_wid=$(xprop -root | grep ^_NET_ACTIVE_WINDOW | awk '{print $NF}')
    if [ -z "$aquarium_wid" ] && [ -n "$active_wid" ]; then
        fullscreen=$(xprop -id "$active_wid" | grep -c _WM_STATE_FULLSCREEN)
    else
        fullscreen=0
    fi

    # Run the script only if you aren't fullscreen or audio isn't playing.
    if [ "$fullscreen" -eq 0 ] && [ "$sound" -eq 0 ]; then
        
        # idleExtra prevents the timer from increasing when playing audio or fullscreen
        # after activity, idleExtra should be reset. Also turn dpms off if it was enabled
        if [ "$idleTime" -le 1 ]; then
            xset q | grep -q Enabled && xset -dpms
            idleExtra=0
        fi
        idleReal=$(( idleTime - idleExtra ))

        # 1st timer - no activity
        # Run asciiquarium
        if  [ "$idleReal" -gt "$timer_1" ] && [ "$screensaver" = "false" ]; then
            st -c aquarium -e asciiquarium >/dev/null 2>&1 &
            screensaver=true
        fi
        
        # 1st timer - activity detected
        # Kill asciiquarium
        if [ "$idleReal" -lt 3 ] && [ "$screensaver" = "true" ]; then
            xdo kill -N aquarium >/dev/null 2>&1
            screensaver=false
        fi
        
        # 2nd timer - no activity
        # Kill asciiquarium, lock the screen and turn the monitor off
        if [ "$idleReal" -gt "$timer_2" ] && [ "$screensaver" = "true" ]; then
            sleep 1; xset dpms force off
            xdo kill -N aquarium >/dev/null 2>&1
            screensaver=false
            slock
        fi
        
    else
        idleExtra="$idleTime"
    fi
    sleep 1      # polling interval

done
