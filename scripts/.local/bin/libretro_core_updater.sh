#! /bin/bash

CORES_DIR=$HOME/Games/retroarch/cores
INFO_DIR=$HOME/Games/retroarch/cores/info

cores=("citra" "dolphin" "swanstation" "fbneo" "flycast" "genesis_plus_gx" "mednafen_ngp" "mednafen_pce_fast" "mednafen_vb" "melonds" "mesen" "mgba" "mupen64plus_next" "nxengine" "pokemini" "ppsspp" "sameboy" "snes9x")

mkdir -p $CORES_DIR
mkdir -p $INFO_DIR
mkdir -p $HOME/.cache/libretro-cores
cd $HOME/.cache/libretro-cores || exit 1

# Download specified cores from latest nightly
for i in ${cores[@]}; do
    wget -A zip -r -nd -np https://buildbot.libretro.com/nightly/linux/x86_64/latest/${i}_libretro.so.zip
done

# Download core info files
wget https://buildbot.libretro.com/assets/frontend/info.zip

# Unzip everything
unzip "*.zip"

# Copy the cores and info files to their respective directories
cp *.so $CORES_DIR
cp *.info $INFO_DIR

# Cleanup
cd ../
rm -rf libretro-cores
echo "Done!"
