#!/bin/sh
# Increase/decrease, or toggle the mute status,
# and send the updated value to the xob named pipe

usage() {
    cat <<EOF
Usage: $0 -i arg | -d arg | -m | -h
where:
    -i increase volume arg%
    -d decrease volume arg%
    -m toggle the mute status
    -h show this help message
EOF
    exit 0
}

while getopts ":i:d:mh" opt; do
    case ${opt} in
        h)  # show the usage
            usage
            ;;

        i)  # increase volume
            pamixer --allow-boost --unmute --increase $OPTARG
            pamixer --get-volume >> /tmp/xob_volume
            ;;
    
        d)  # decrease volume
            pamixer --allow-boost --unmute --decrease $OPTARG
            pamixer --get-volume >> /tmp/xob_volume
            ;;
    
        m)  # toggle mute
            pamixer --toggle-mute
            if $(pamixer --get-mute); then
                pamixer --get-volume | sed 's/$/\!/' >> /tmp/xob_volume
            else
                pamixer --get-volume >> /tmp/xob_volume
            fi
            ;;

        \?) # unknown optarg
            echo "Invalid option: -$OPTARG" 1>&2
            exit 1
            ;;

        : )  # optarg without argument
            echo "Option -"$OPTARG" requires an argument." 1>&2
            exit 1
            ;;
    esac
done
