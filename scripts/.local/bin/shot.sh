#!/bin/sh

usage() {
    echo 'Options:'
    echo
    echo '  -h'
    echo '          Show this help message.'
    echo
    echo
    echo '  -d' 
    echo '          Takes a screenshot of the entire desktop.'
    echo
    echo
    echo '  -D'
    echo '          Takes a screenshot of the entire desktop'
    echo '          and copies it to the clipboard.'
    echo
    echo
    echo '  -w'
    echo '          Takes a screenshot of the current focused window.'
    echo
    echo
    echo '  -W'
    echo '          Takes a screenshot of the current focused window'
    echo '          and copies it to the clipboard.'
    echo
    echo
    echo '  -s'
    echo '          Takes a screenshot of the area selected by the user.'
    echo
    echo
    echo '  -S'
    echo '          Takes a screenshot of the area selected by the user'
    echo '          and copies it to the clipboard.'
}

send_notification() {
    if [ $# -eq 0 ]; then
        notify-send -t 5000 -i ~/.local/share/icons/camera.png -u normal \
            -h string:x-canonical-private-synchronous:clipshot "Screenshot!" "Copied to the clipboard"
    else
        notify-send -t 5000 -i ~/.local/share/icons/camera.png -u normal \
            -h string:x-canonical-private-synchronous:shot "$1" "Saved to ~/Pictures/screenshots"
    fi
}

# Options
while getopts ":hwWsSdD" opt; do
    case ${opt} in
        h)
            usage
            ;;

        w)
            # Active window selection
	    mkdir -p ~/Pictures/screenshots
            name="screenshot_w_$(date +%Y%m%d_%H%M%S).webp"
            import -window $(xdo id) -define webp:lossless=true ~/Pictures/screenshots/"$name"
	    send_notification "$name"
	    ;;

        W)
            # Copy active window to the clipboard
            import -window $(xdo id) png:- | xclip -t 'image/png' -selection clipboard
            send_notification
	    ;;

        s)
            # Select a custom area
	    mkdir -p ~/Pictures/screenshots
            name="screenshot_s_$(date +%Y%m%d_%H%M%S).webp"
            xdo pointer_motion -x +0 -y +0 #show cursor if it's hidden
            import -define webp:lossless=true ~/Pictures/screenshots/"$name"
	    send_notification "$name"
	    ;;

        S)
            # Copy custom area to the clipboard
            xdo pointer_motion -x +0 -y +0 #show cursor if it's hidden
            import png:- | xclip -t 'image/png' -selection clipboard
            send_notification
	    ;;

        D)
            # Copy the whole screen to the clipboard
            import -window root png:- | xclip -t 'image/png' -selection clipboard
            send_notification
	    ;;

        d)
            # Takes a screenshot of the whole screen
	    mkdir -p ~/Pictures/screenshots
            name="screenshot_$(date +%Y%m%d_%H%M%S).webp"
            import -window root -define webp:lossless=true ~/Pictures/screenshots/"$name"
	    send_notification "$name"
	    ;;

        \?)
            echo "Invalid option: -$OPTARG" 1>&2
            ;;
    esac
done
