#!/bin/sh

# This is basically mpdq (https://github.com/uriel1998/mpdq) with very basic features.
# Sort of a "party mode" that will only be active with repeat and random off and consume on.
# When a song finishes playing, a new random one will be added.

QUEUE=5 # the max number of songs that the playlist will have
PIDFILE=/tmp/mpdrand.pid

[ -f "$PIDFILE" ] || touch "$PIDFILE"

# Check if there's already an open instance.
if pgrep -F "$PIDFILE" >/dev/null; then
    printf "%s\n" "Already running." >&2
    exit 1
fi

# Store the pid
echo "$$" > "$PIDFILE"

(echo ""; mpc idleloop) | while read -r line; do
    if [ ! -f "$PIDFILE" ]; then
        exit 1
    fi

    # If MPD is not set to repeat: off, random: off, and consume: on, just wait for the next idle loop
    pauseme=$(mpc status [%repeat%%random%%consume%])
    if [ "$pauseme" = "offoffon" ]; then
        playlist_length=$(mpc playlist | wc -l)
        if [ "$playlist_length" -lt "$QUEUE" ]; then
            mpc listall | shuf -n 1 | mpc add
        fi
    fi
done
