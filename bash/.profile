#
# ~/.profile
#

# Make ~/.local/bin folder executable
export PATH="$PATH:$HOME/.local/bin"

# XDG Base Directory
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# Default programs
export EDITOR='vim'
export VISUAL='vim'
export TERMINAL='st'
export BROWSER='qutebrowser'

# FZF
export FZF_DEFAULT_OPTS="--bind change:first --inline-info"
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"
export FZF_DEFAULT_COMMAND='find "$HOME" \! \( \( -name ".git" \
    -o -name ".cache" -o -name ".wine" -o -name "proton" \
    -o -name "Steam" -o -name "steamapps" \) -prune \)'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="$FZF_DEFAULT_COMMAND -type d"

# Fcitx
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

# ninfs
export BOOT9_PATH="$XDG_DATA_HOME"/3ds
export SEEDDB_PATH="$XDG_DATA_HOME"/3ds

# Other
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export HISTFILE="$XDG_STATE_HOME"/bash/history
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export LESSHISTFILE="$XDG_STATE_HOME"/less/history
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export WGETRC="$XDG_CONFIG_HOME"/wgetrc
export WINEPREFIX="$XDG_DATA_HOME"/wine
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority

# Fix misbehaving Java applications on dwm
export _JAVA_AWT_WM_NONREPARENTING=1
