#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PROMPT_COMMAND=__prompt_command

__prompt_command() {
    local EXIT=${PIPESTATUS[-1]}
    PS1=""

    # vifm instance
    if [ -n "$INSIDE_VIFM" ]; then
        PS1+="\[\e[0;35m\][$INSIDE_VIFM] \[\e[0m\]"
    fi

    # exit code
    if [ ${EXIT} != 0 ]; then
        PS1+="\[\e[0;31m\]${EXIT}\[\e[0m\]"
    fi

    # user, host and current directory
    PS1+='\[\e[0;36m\]\u\[\e[0;35m\]@\[\e[0;36m\]\h\[\e[1;37m\]: \[\e[0;33m\]\w'

    # git branch
    if [[ $(command -v git) ]]; then
        local BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
        if [ ! "${BRANCH}" == "" ]; then
            PS1+="\[\e[0;32m\] ( ${BRANCH})"
        fi
    fi

    # rest of the prompt
    PS1+="\[\e[0m\]\n$ "

    # If this is an xterm set the titlebar to user@host: dir
    case "$TERM" in
    xterm*|rxvt*|st*)
        PS1="\[\e]2;\u@\h: \w\a\]$PS1"
        ;;
    *)
        ;;
    esac

    # new line before every prompt except the first time
    prompt_newline
}

prompt_newline() {
    prompt_newline() {
        echo
    }
}

# vi mode in bash
set -o vi
bind 'set show-mode-in-prompt on'
bind 'set vi-ins-mode-string +'
bind 'set vi-cmd-mode-string -'

# completion
bind 'TAB:menu-complete'
bind 'set show-all-if-ambiguous on'

# bash history
HISTSIZE=10000
HISTFILESIZE=10000
HISTCONTROL=ignoredups:erasedups
shopt -s histappend

# Disable ctrl-s and ctrl-q
stty -ixon

# fzf
source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash

# icons-in-terminal
source "$XDG_DATA_HOME"/icons-in-terminal/icons_bash.sh

# aliases
alias bc='bc -q -l'
alias cp='cp -iv'
alias diff='diff --color=auto'
alias ffmpeg='ffmpeg -hide_banner'
alias grep='grep -i --color=auto'
alias ls='ls -hN --color=auto --group-directories-first'
alias mkdir='mkdir -pv'
# go to attach dir to save attachments there
alias mv='mv -iv'
alias pcache='paru -Sc'
alias please='sudo' # polite bash
alias porphan='paru -Rns $(paru -Qtdq)'
alias rm='rm -Iv'
alias vifm='vifmrun'
alias weather='curl wttr.in'
alias wget='wget --hsts-file="$XDG_DATA_HOME"/wget-hsts'
alias yay='paru'

# custom functions
note () {
    #if file doesn't exist, create it
    [ -f "$XDG_DATA_HOME"/notes ] || touch "$XDG_DATA_HOME"/notes

    #no arguments, print file
    if [ $# = 0 ]
        then
            cat "$XDG_DATA_HOME"/notes
    #clear file
    elif [ $1 = -c ]
        then
            > "$XDG_DATA_HOME"/notes
    #add all arguments to file
    else
        echo "$@" >> "$XDG_DATA_HOME"/notes
    fi
}

#bu - Back Up a file. Usage "bu filename.txt"
bu () { 
    cp $1 ${1}-`date +%Y%m%d%H%M`.backup ;
}

# (mis)using the DEBUG signal to change the terminal title to the current command
trap 'echo -ne "\033]2;$(history 1 | sed "s/^[ ]*[0-9]*[ ]*//g")\007"' DEBUG
