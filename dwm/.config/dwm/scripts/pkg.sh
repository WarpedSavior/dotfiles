#!/bin/sh

wget -q --spider http://www.archlinux.org >/dev/null 2>&1

if [ $? -eq 0 ]; then
    pac=$(checkupdates 2> /dev/null | wc -l)
    aur=$(paru -Qum 2> /dev/null | wc -l)
    updates=$(( pac + aur ))

    notify-send -t 5000 -u normal "Number of available updates:" \
        "$pac updates from official repos.\n$aur updates from AUR."
else
    notify-send -t 5000 -u normal "You are offline" \
        "The package list can't be retrieved at this moment."
fi
