#! /bin/sh

while getopts "zrc" opt; do
    case ${opt} in
        z) # toggle random mode
            mpc --quiet random

            random=$(mpc status %random%)
            notify-send -t 5000 -u normal -i "audio-x-generic" \
                -h string:x-canonical-private-synchronous:mpd-modes \
                "MPD Notification" "Random mode is $random"
            ;;
        r) # toggle repeat-single mode
            repeat_single=$(mpc status [%repeat%%single%])

            # there's a weird interaction when repeat, single and consume are all on
            mpc --quiet consume off

            if [ "$repeat_single" = "onon" ]; then
                mpc --quiet repeat off; mpc --quiet single off
                notify-send -t 5000 -u normal -i "audio-x-generic" \
                    -h string:x-canonical-private-synchronous:mpd-modes \
                    "MPD Notification" "Repeat-Single mode is off"
            else
                mpc --quiet repeat on; mpc --quiet single on
                notify-send -t 5000 -u normal -i "audio-x-generic" \
                    -h string:x-canonical-private-synchronous:mpd-modes \
                    "MPD Notification" "Repeat-Single mode is on"
            fi
            ;;
        c) # toggle consume mode
            repeat_single=$(mpc status [%repeat%%single%])
            mpc --quiet consume

            # there's a weird interaction when repeat, single and consume are all on
            if [ "$repeat_single" = "onon" ]; then
                mpc --quiet repeat off; mpc --quiet single off
            fi

            consume=$(mpc status %consume%)
            notify-send -t 5000 -u normal -i "audio-x-generic" \
                -h string:x-canonical-private-synchronous:mpd-modes \
                "MPD Notification" "Consume mode is $consume"
            ;;
        *)
            printf "%b\n" "Invalid option: $OPTARG" 1>&2
            ;;
    esac
done
