#!/bin/sh

. $HOME/.config/dwm/icons/icons_weather

weather=$(curl -s 'wttr.in/?format=%t')

if [ $? -eq 0 ]; then

    # Check if wttr.in is down but still reports Unknown location
    if test "${weather#*'Unknown location'}" != "$weather"; then
        printf "%s\n" "WTR"
        exit 1
    fi

    hour=$(date +%H)
    hour_decimal=${hour#0}
    weather_condition=$(curl -s 'wttr.in/?format=%C' )

    if [ "$hour_decimal" -ge 20 ] || [ "$hour_decimal" -le 7 ]; then
        case $weather_condition in
            'Overcast') icon=$CLOUDY;;
            'Freezing fog') icon=$CLOUDY_FOG;;
            'Ice pellets') icon=$PELLETS_NIGHT;;
            'Thundery outbreaks possible') icon=$THUNDER_NIGHT;;
            *[Ll]ight*rain) icon=$LIGHT_RAIN_NIGHT;;
            'Patchy rain possible') icon=$LIGHT_RAIN_NIGHT;;
            *[Hh]eavy*rain*) icon=$HEAVY_RAIN_NIGHT;;
            'Moderate or heavy sleet showers') icon=$HEAVY_RAIN_NIGHT;;
            'Torrential rain shower') icon=$HEAVY_RAIN_NIGHT;;
            Moderate*rain*) icon=$RAIN_NIGHT;;
            Rain) icon=$RAIN_NIGHT;;
            *[Dd]rizzle*) icon=$SHOWERS_NIGHT;;
            Light\ rain*) icon=$SHOWERS_NIGHT;;
            'Light sleet showers') icon=$SHOWERS_NIGHT;;
            Light\ snow*) icon=$SNOW_NIGHT;;
            'Patchy light snow') icon=$SNOW_NIGHT;;
            'Moderate snow') icon=$SNOW_NIGHT;;
            'Patchy moderate snow') icon=$SNOW_NIGHT;;
            *rain\ with\ thunder) icon=$THUNDERSTORM_NIGHT;;
            *[Tt]hunderstorm*) icon=$THUNDERSTORM_NIGHT;;
            Clear|Sunny) icon=$CLEAR_NIGHT;;
            Fog|*[Mm]ist*) icon=$FOG_NIGHT;;
            'Shallow fog') icon=$FOG_NIGHT;;
            Blizzard) icon=$BLIZZARD;;
            'Blowing snow') icon=$HEAVY_SNOW_NIGHT;;
            *[Hh]eavy\ snow) icon=$HEAVY_SNOW_NIGHT;;
            'Moderate or heavy snow showers') icon=$HEAVY_SNOW_NIGHT;;
            *snow\ with\ thunder) icon=$THUNDER_SNOW_NIGHT;;
            *[Cc]loudy) icon=$CLOUDY_NIGHT;;
            'Patchy snow possible') icon=$SLEET_NIGHT;;
            'Patchy sleet possible') icon=$SLEET_NIGHT;;
            *sleet) icon=$SLEET_NIGHT;;
            *) icon=$ERROR;;
        esac
    else
        case $weather_condition in
            *[Cc]loudy) icon=$CLOUDY_DAY;;
            Fog|*[Mm]ist*) icon=$FOG_DAY;;
            'Shallow fog') icon=$FOG_DAY;;
            'Ice pellets') icon=$PELLETS_DAY;;
            'Thundery outbreaks possible') icon=$THUNDER_DAY;;
            *[Ll]ight*rain) icon=$LIGHT_RAIN_DAY;;
            'Patchy rain possible') icon=$LIGHT_RAIN_DAY;;
            *[Hh]eavy*rain*) icon=$HEAVY_RAIN_DAY;; #rain wind
            'Moderate or heavy sleet showers') icon=$HEAVY_RAIN_DAY;;
            'Torrential rain shower') icon=$HEAVY_RAIN_DAY;;
            Moderate*rain*) icon=$RAIN_DAY;;
            Rain) icon=$RAIN_DAY;;
            *[Dd]rizzle*) icon=$SHOWERS_DAY;;
            Light\ rain*) icon=$SHOWERS_DAY;;
            'Light sleet showers') icon=$SHOWERS_DAY;;
            Light\ snow*) icon=$SNOW_DAY;;
            'Patchy light snow') icon=$SNOW_DAY;;
            'Moderate snow') icon=$SNOW_DAY;;
            'Patchy moderate snow') icon=$SNOW_DAY;;
            Clear|Sunny) icon=$CLEAR_DAY;;
            *rain\ with\ thunder) icon=$THUNDERSTORM_DAY;;
            *[Tt]hunderstorm*) icon=$THUNDERSTORM_DAY;;
            'Overcast') icon=$CLOUDY;;
            'Freezing fog') icon=$CLOUDY_FOG;;
            'Blizzard') icon=$BLIZZARD;;
            'Blowing snow') icon=$HEAVY_SNOW_DAY;;
            *[Hh]eavy\ snow) icon=$HEAVY_SNOW_DAY;;
            'Moderate or heavy snow showers') icon=$HEAVY_SNOW_DAY;;
            *snow\ with\ thunder) icon=$THUNDER_SNOW_DAY;;
            'Patchy snow possible') icon=$SLEET_DAY;;
            'Patchy sleet possible') icon=$SLEET_DAY;;
            *sleet) icon=$SLEET_DAY;;
            *) icon=$ERROR;;
        esac
    fi
    printf "%s\n" "WTR\007 ${icon} ${weather} |"
else
    printf "%s\n" "WTR"
fi
